library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity test_instruction_register is
end entity test_instruction_register;

architecture test of test_instruction_register is 
  signal IR_IN : std_logic_vector(31 downTo 0);
  signal IR_OUT : std_logic_vector(31 downTo 0);
  signal WRITE : bit;
begin
  dut : entity work.instruction_register(behav)
    port map (IR_IN, IR_OUT, WRITE);
  stimulus : process is
  begin
    WRITE <= '0';
    wait for 2 ns;
    assert IR_OUT = "00000000000000000000000000000000" report "IR is not initalized properly to zeros" severity warning;
    
    WRITE <= '0';
    IR_IN <= "11111111111111111111111111111111";
    wait for 2 ns;
    assert IR_OUT = "00000000000000000000000000000000" report "IR is being set when write is disabled" severity warning;
    
    WRITE <= '1';
    IR_IN <= "11111111111111111111111111111111";
    wait for 2 ns;
    assert IR_OUT = "11111111111111111111111111111111" report "IR is not properly being set" severity warning;
       
    
    wait;
    
  end process stimulus;
end architecture test;



