library IEEE;
use IEEE.std_logic_1164.all;

entity test_alu is
end entity test_alu;

architecture test of test_alu is
  
  signal A : std_logic_vector(31 downto 0) := (others => '0');
  signal B : std_logic_vector(31 downto 0) := (others => '0');
  signal CHOICE : std_logic_vector(1 downto 0) := (others => '0');
  signal B_SHIFT : bit := '0';
  signal RESULT : std_logic_vector (31 downto 0) := (others => '0');
  signal ZERO_DETECT : bit := '0';

  -- Operation 
  constant O_ADD : std_logic_vector(1 downto 0) := "00";
  constant O_SUB : std_logic_vector(1 downto 0) := "01";
  constant O_OR : std_logic_vector(1 downto 0) := "10";
  constant O_AND : std_logic_vector(1 downto 0) := "11";
    
begin
  dut : entity work.ALU_32(ALU)
    port map ( A, B, CHOICE, B_SHIFT, RESULT, ZERO_DETECT);
  stimulus : process is
  begin
    A <= "00000000000000000000000000000010";
    B <= "00000000000000000000000000000001";
    wait for 2 ns;
    CHOICE <= O_ADD;
    B_SHIFT <= '0';
    wait for 2 ns;
    assert RESULT = "00000000000000000000000000000011" report "ADD Operation failed" severity error;  

    A <= "00000000000000000000000000000011";
    B <= "00000000000000000000000000000001";
    CHOICE <= O_SUB;
    B_SHIFT <= '0';
    wait for 5 ns;
    assert RESULT = "00000000000000000000000000000010" report "SUB Operation failed" severity error;  
        
    A <= "00000000000000000000000000000011";
    B <= "00000000000000000000000000000010";
    CHOICE <= O_AND;
    B_SHIFT <= '0';
    wait for 5 ns;
    assert RESULT = "00000000000000000000000000000010" report "AND Operation failed" severity error;  
                
    A <= "00000000000000000000000000000011";
    B <= "00000000000000000000000000000101";
    CHOICE <= O_OR;
    B_SHIFT <= '0';
    wait for 5 ns;
    assert RESULT = "00000000000000000000000000000111" report "OR Operation failed" severity error;  
                
    A <= "00000000000000000000000000000011";
    B <= "00000000000000000000000000000001";
    wait for 2 ns;
    CHOICE <= O_ADD;
    B_SHIFT <= '1';
    wait for 2 ns;
    assert RESULT = "00000000000000000000000000000111" report "ADD with B Shift Operation failed" severity error;  
           
    
  end process stimulus;
end architecture test;
