library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity test_memory is
end entity test_memory;

architecture behav of test_memory is
  signal mem_write : bit := '0';
  signal address : std_logic_vector(31 downTo 0) := "00000000000000000000000000000011";
  signal data_in : std_logic_vector(31 downTo 0) := "11111111111111111111111111111111";
  signal data_out : std_logic_vector(31 downTo 0);
begin
  mem_component : entity work.memory(behav) port map (mem_write, address, data_in, data_out);
  stimulus : process is
  begin   
    wait for 4 ns;
    -- Correct default program data
    address <= "00000000000000000000000000000000"; -- 0
    wait for 4 ns;
    assert data_out = "10001100000000000000000000000000" report "Instruction 1 not loaded" severity warning;
 
    address <= "00000000000000000000000000000100"; -- 4
    wait for 4 ns;
    assert data_out = "10101100000000000000000000000000" report "Instruction 2 not loaded" severity warning;
       
    address <= "00000000000000000000000000001000"; -- 8
    wait for 4 ns;
    assert data_out = "10000000000000000000000000000000" report "Instruction 3 not loaded" severity warning;
        
    
    address <= "00000000000000000000000000000000";
    data_in <= "11111111111111111111111111111111";
    mem_write <= '1';
    wait for 4 ns;
    assert data_out = "11111111111111111111111111111111" report "Memory not properly setting addresss 1" severity warning;
    
    
    address <= "00000000000000000000000000000100";
    data_in <= "11000000000000000000000000000000";
    mem_write <= '1';
    wait for 4 ns;
    assert data_out = "11000000000000000000000000000000" report "Memory not properly setting addresss 2" severity warning;
        

    mem_write <= '0';
    address <= "00000000000000000000000010000000";
    data_in <= "11111111111111111111111111111111";
    wait for 4 ns;
    assert data_out /= "11111111111111111111111111111111" report "Memory writing when disabled" severity warning;
    
    mem_write <= '1';
    address <= "00000000000000000000000111111100";
    data_in <= "11111111111111111111111111111010";
    wait for 4 ns;
    assert data_out = "11111111111111111111111111111010" report "Can not access last address in memory" severity warning;
    
    wait;
    
  end process stimulus;
end architecture behav;