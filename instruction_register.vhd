library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.Numeric_Std.all;


entity instruction_register is
  port (
    data_in : in std_logic_vector(31 downTo 0);
    data_out : out std_logic_vector(31 downTo 0) := "00000000000000000000000000000000";
    write : in bit
  );
end instruction_register;

architecture behav of instruction_register is
  begin
    main_process : process(data_in, write) is
    begin
        if write = '1' then
          data_out <= data_in;
        end if;
  end process main_process;
  
end architecture behav;


