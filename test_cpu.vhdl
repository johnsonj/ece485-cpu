library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity test_cpu is
end entity test_cpu;

architecture behav of test_cpu is 
  signal RUN : bit;
  signal PC : std_logic_vector(31 downTo 0);
  signal PC_in : std_logic_vector(31 downTo 0);
  signal PC_load : bit;
  signal IR : std_logic_vector(31 downTo 0);
  signal MEM_ADDRESS : std_logic_vector(31 downTo 0) := "00000000000000000000000000000000";
  signal MEM_DATA : std_logic_vector(31 downTo 0);
  signal REG_1 : std_logic_vector(4 downTo 0) := "00000";
  signal REG_2 : std_logic_vector(4 downTo 0) := "00000";
  signal REG_1_OUT : std_logic_vector(31 downTo 0);
  signal REG_2_OUT : std_logic_vector(31 downTo 0);
  signal RESET : bit;

begin
  dut : entity work.cpu(behav)
    port map (RUN, PC, PC_in, PC_load, IR, MEM_ADDRESS, MEM_DATA, RESET, REG_1, REG_2, REG_1_OUT, REG_2_OUT);
  stimulus : process is
  begin
    RUN <= '0';
    -- Check our sample program is loaded
    MEM_ADDRESS <= "00000000000000000000000000000000";
    wait for 4 ns;
    assert MEM_DATA = "10001101010100110000000001100100" report "Program is not loaded" severity warning;
    
    
    
    -- Run CPU for 1 cycle - lw
    -- r19 <= mem(101 << 2)
    -- r19 == 00010001101100100000001001011000
    RUN <= '1';
    wait for 5 ns;
    RUN <= '0';
    
    REG_2 <= "10011"; -- r19
    wait for 50 ns;

    assert IR = "10001101010100110000000001100100" report "Instruction 1 not properly loaded into IR" severity warning;
    assert REG_2_OUT = "00010001101100100000001001011000" report "LW did not properly set R19" severity warning;    
    
    RESET <= '1';
    wait for 5 ns;
    RESET <= '0';
    
    
    
    -- Run CPU for 1 cycle - sw
    -- mem(rs + im) <= rt
    -- mem((r13 + 200) << 2) <= r20
    -- mem((3 + 200) << 2) <= 8   
    -- mem(00000000000000000000001100101100) <= 8
    RUN <= '1';
    wait for 5 ns;
    RUN <= '0';
    
    MEM_ADDRESS <= "00000000000000000000001100101100";
    wait for 50 ns;
    
    assert IR = "10101101101101000000000011001000" report "Instruction 2 not properly loaded into IR" severity warning;
    assert MEM_DATA = "00000000000000000000000000001000" report "SW did not properly set memory location" severity warning;

    RESET <= '1';
    wait for 5 ns;
    RESET <= '0';

    -- Run CPU for 1 cycle - add
    -- r11 <= r10 + r18
    -- r11 <= 1 + 6
    
    RUN <= '1'; wait for 5 ns; RUN <= '0';
    
    REG_2 <= "01011";
    wait for 50 ns;
    
    assert IR = "10000001010100100101100000000000" report "Instruction 3 not properly loaded into IR" severity warning;
    assert REG_2_OUT = "00000000000000000000000000000111" report "ADD did not properly set R11" severity warning;

    RESET <= '1';
    wait for 5 ns;
    RESET <= '0';    
    
    
    -- Run CPU for 1 cycle - or
    
    -- rd <= rs || rt
    -- r13 <= r22 || r13
    -- r13 <= 9 || 3, 1001 || 11
    -- r13 <= 00000000000000000000000000001011
    RUN <= '1'; wait for 5 ns; RUN <= '0';
    
    REG_2 <= "01101";
    wait for 50 ns;
    
    assert IR = "10010110110011010110100000000000" report "Instruction 4 not properly loaded into IR" severity warning;
    assert REG_2_OUT = "00000000000000000000000000001011" report "OR did not properly set R13" severity warning;

    RESET <= '1';
    wait for 5 ns;
    RESET <= '0';        
    
     -- Run CPU for 1 cycle - slt
    -- rd <= (rs < rt)
    -- r14 <= (r17 < r12)
    -- r14 <= 0
    
    RUN <= '1'; wait for 5 ns; RUN <= '0';
    
    REG_2 <= "01110";
    wait for 50 ns;
    
    assert IR = "10101010001011000111000000000000" report "Instruction 5 not properly loaded into IR" severity warning;
    assert REG_2_OUT = "00000000000000000000000000000000" report "SLT did not properly set R14" severity warning;

    RESET <= '1';
    wait for 5 ns;
    RESET <= '0';
    
     -- Run CPU for 1 cycle - beq
    -- PC <= (PC + 4) + (600 << 2) if R18 == R18 (it should)
    -- PC <= 00000000000000000000100101111000
    
    RUN <= '1'; wait for 5 ns; RUN <= '0';
    wait for 50 ns;
    
    assert IR = "00010010010100100000001001011000" report "Instruction 6 not properly loaded into IR" severity warning;
    assert PC = "00000000000000000000100101111000" report "BEQ did not properly set PC" severity warning;

    RESET <= '1';
    wait for 5 ns;
    RESET <= '0';    
    
    -- Reset the PC back to 24
    PC_in <= "00000000000000000000000000011000";
    PC_load <= '1';
    wait for 5 ns;
    PC_load <= '0';
    
    
    -- Run CPU for 1 cycle - j
    -- PC <= (PC + 4)(31:28) + (address << 2)
    -- PC <= (11100)(31:28) + (01100 << 2)
    -- PC <= (0110000)
    RUN <= '1'; wait for 5 ns; RUN <= '0';
    
    wait for 50 ns;
    
    assert IR = "00001000000000000000000000001100" report "Instruction 7 not properly loaded into IR" severity warning;
    assert PC = "00000000000000000000000000110000" report "J did not properly set PC" severity warning;

    RESET <= '1';
    wait for 5 ns;
    RESET <= '0';    
    
    -- Reset the PC back to 28
    PC_in <= "00000000000000000000000000011100";
    PC_load <= '1';
    wait for 5 ns;
    PC_load <= '0';
    
    
    -- Run CPU for 1 cycle - ori
    -- rt <= rs || imm
    -- r14 <= r17 || 01100100
    -- r14 <= 00101 || 01100100
    -- r14 <= 01100101
    RUN <= '1'; wait for 5 ns; RUN <= '0';
    
    REG_2 <= "01110";
    wait for 50 ns;
    
    assert IR = "00110110001011100000000001100100" report "Instruction 8 not properly loaded into IR" severity warning;
    assert REG_2_OUT = "00000000000000000000000001100101" report "ORI did not properly set R14" severity warning;

    RESET <= '1';
    wait for 5 ns;
    RESET <= '0';    
    
     -- Run CPU for 1 cycle - andi
    -- rt <= rs & imm
    -- r15 <= r17 & 01100100
    -- r15 <= 00101 & 01100100
    -- r15 <= 00100
    RUN <= '1'; wait for 5 ns; RUN <= '0';
    
    REG_2 <= "01111";
    wait for 50 ns;
    
    assert IR = "00110010001011110000000001100100" report "Instruction 9 not properly loaded into IR" severity warning;
    assert REG_2_OUT = "00000000000000000000000000000100" report "ANDI did not properly set R15" severity warning;

    RESET <= '1';
    wait for 5 ns;
    RESET <= '0';                 

    -- Run CPU for 1 cycle - lui
    -- rt <= imm << 16
    -- r22 <= 1100100 << 16
    -- r22 <= 00000000011001000000000000000000
    
    RUN <= '1'; wait for 5 ns; RUN <= '0';
    
    REG_2 <= "10110";
    wait for 50 ns;
    
    assert IR = "00111100000101100000000001100100" report "Instruction 10 not properly loaded into IR" severity warning;
    assert REG_2_OUT = "00000000011001000000000000000000" report "LUI did not properly set R15" severity warning;

    RESET <= '1';
    wait for 5 ns;
    RESET <= '0';     

     -- Run CPU for 1 cycle - jr
    -- PC <= rs
    -- PC <= r14
    -- PC <= 4
    
    RUN <= '1'; wait for 5 ns; RUN <= '0';
    wait for 50 ns;
    
    assert IR = "00100001110000000000000000000000" report "Instruction 11 not properly loaded into IR" severity warning;
    assert PC = "00000000000000000000000000000100" report "JR did not properly set PC" severity warning;

    RESET <= '1';
    wait for 5 ns;
    RESET <= '0';        
    wait;
  end process stimulus;
end architecture behav;

