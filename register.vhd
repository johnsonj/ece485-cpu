library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.Numeric_Std.all;

entity registers is
  port (
    reg1 : in std_logic_vector(4 downto 0);
    reg2 : in std_logic_vector(4 downto 0);
    writeReg : in std_logic_vector(4 downto 0);
    writeData : in std_logic_vector (31 downto 0);
    writeEnabled : in bit; -- write = '1'
    data_out1 : out std_logic_vector(31 downto 0);
    data_out2 : out std_logic_vector(31 downto 0);
    reset : in bit := '0');
    
end registers;

architecture behav of registers is
  
  type storage_type is array (0 to 31) of std_logic_vector(31 downto 0);
  signal registers : storage_type := (
    10 => "00000000000000000000000000000001", --r10 = 1
    11 => "00000000000000000000000000000010", --r11 = 2
    13 => "00000000000000000000000000000011", --r13 = 3
    14 => "00000000000000000000000000000100", --r14 = 4
    17 => "00000000000000000000000000000101", --r17 = 5
    18 => "00000000000000000000000000000110", --r18 = 6
    19 => "00000000000000000000000000000111", --r19 = 7
    20 => "00000000000000000000000000001000", --r20 = 8
    22 => "00000000000000000000000000001001", -- r22 = 9
    OTHERS=>"00000000000000000000000000000000"
  );
  
  begin
    cycle: process (reg1, reg2, writeReg, writeData, writeEnabled, reset) is begin
      if writeEnabled = '1' then
        registers(to_integer(unsigned(writeReg))) <= writeData;
        data_out1 <= registers(to_integer(unsigned(reg1)));
        data_out2 <= registers(to_integer(unsigned(reg2)));
      else
        data_out1 <= registers(to_integer(unsigned(reg1)));
        data_out2 <= registers(to_integer(unsigned(reg2)));     
      end if;
      
      if reset = '1' then
          registers(10) <= "00000000000000000000000000000001";
          registers(11) <= "00000000000000000000000000000010";
          registers(13) <= "00000000000000000000000000000011";
          registers(14) <= "00000000000000000000000000000100"; 
          registers(17) <= "00000000000000000000000000000101"; 
          registers(18) <= "00000000000000000000000000000110"; 
          registers(19) <= "00000000000000000000000000000111"; 
          registers(20) <= "00000000000000000000000000001000"; 
          registers(22) <= "00000000000000000000000000001001";   
        end if;     
    end process;
end architecture behav;  
    
