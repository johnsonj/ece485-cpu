library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity ALU_32 is
  port( A: in std_logic_vector (31 downto 0);
        B: in std_logic_vector (31 downto 0);
        CHOICE: in std_logic_vector (1 downto 0);
        B_SHIFT: in bit;
        RESULT: out std_logic_vector (31 downto 0);
        ZERO_DETECT: out bit);
end ALU_32;

architecture ALU of ALU_32 is
  
  --signal ALU_TMP : std_logic_vector (31 downto 0) := "00000000000000000000000000000000";
  --signal B_TMP : std_logic_vector (31 downto 0) := (others => '0');
  -- Operation 
  constant O_ADD : std_logic_vector(1 downto 0) := "00";
  constant O_SUB : std_logic_vector(1 downto 0) := "01";
  constant O_OR : std_logic_vector(1 downto 0) := "10";
  constant O_AND : std_logic_vector(1 downto 0) := "11";
  
  -- B_SHIFT
  constant SHIFT_ACT : bit := '1';
  
  begin
   
    -- Operation
    process (CHOICE, A, B, B_SHIFT)
      variable ALU_TMP : std_logic_vector(31 downto 0) := (others => '0');
      variable B_TMP : std_logic_vector(31 downto 0) := (others => '0');
      begin
          -- B_SHIFT implement
          case B_SHIFT is
            when SHIFT_ACT => 
              --B_TMP <= B(29 downto 0) & "00";
              B_TMP := std_logic_vector(SHIFT_LEFT(unsigned(B),2));
            when '0' => 
              --B_TMP <= B(31 downto 0);
              B_TMP := B;
          end case;
        
          case CHOICE is
            when O_ADD => 
              ALU_TMP := std_logic_vector(unsigned(A) + unsigned(B_TMP));
              --ALU_TMP := unsigned(A) + B_TMP;
            when O_SUB => 
              ALU_TMP := std_logic_vector(unsigned(A) - unsigned(B_TMP));
              --ALU_TMP := unsigned(A) - B_TMP;
            when O_OR => 
              ALU_TMP := std_logic_vector((A or B_TMP));
              --ALU_TMP := unsigned(A) or B_TMP;
            when O_AND => 
              ALU_TMP := std_logic_vector(A and B_TMP);
              --ALU_TMP := unsigned(A) and B_TMP;
            when others => null;
          end case;
          
       -- Determine Zero or Not     
          if(ALU_TMP = "00000000000000000000000000000000") then
            ZERO_DETECT <= '1';
          else 
            ZERO_DETECT <= '0';
          end if;          
        
        RESULT <= (ALU_TMP);              
      end process;
    
end ALU;