library IEEE;
use IEEE.std_logic_1164.all;

entity test_reg is
end entity test_reg;

architecture test of test_reg is
  signal reg1 : std_logic_vector(4 downto 0) := (others => '0');
  signal reg2 : std_logic_vector(4 downto 0) := (others => '0');
  signal writeReg : std_logic_vector(4 downto 0) := (others => '0');
  signal writeData : std_logic_vector(31 downto 0) := (others => '0');
  signal writeEnabled : bit := '0';
  signal data_out1 : std_logic_vector(31 downto 0) := (others => '0');
  signal data_out2 : std_logic_vector(31 downto 0) := (others => '0');
  
  begin
    dut2 : entity work.registers(behav)
      port map (reg1, reg2, writeReg, writeData, writeEnabled, data_out1, data_out2);
        stimulus : process is
          begin
            wait for 10 ns;
            reg1 <= "01010";
            reg2 <= "10010";
            writeEnabled <= '1';
            writeReg <= "01010";
            writeData <= "00000000000000000000000000000101";
            
            wait for 5 ns;
            assert data_out1 = "00000000000000000000000000000101" report "OUTPUT1 is something wrong" severity error;
            assert data_out2 = "00000000000000000000000000000110" report "OUTPUT2 is something wrong" severity error;
            
          end process stimulus;
end architecture test;