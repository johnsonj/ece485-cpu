library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.Numeric_Std.all;


entity cpu is
  port (
    SIMULATOR_RUN : in bit;
    SIMULATOR_PC : out std_logic_vector(31 downTo 0);
    SIMULATOR_PC_in : in std_logic_vector(31 downTo 0);
    SIMULATOR_PC_write : in bit;
    SIMULATOR_IR : out std_logic_vector(31 downTo 0);
    SIMULATOR_MEM_ADDRESS : in std_logic_vector(31 downTo 0);
    SIMULATOR_MEM_DATA : out std_logic_vector(31 downTo 0);
    SIMULATOR_RESET : in bit;
    SIMULATOR_REG_1 : in std_logic_vector(4 downto 0);
    SIMULATOR_REG_2 : in std_logic_vector(4 downto 0);
    SIMULATOR_REG_1_OUT : out std_logic_vector(31 downTo 0);
    SIMULATOR_REG_2_OUT : out std_logic_vector(31 downTo 0)
  );
end cpu;

architecture behav of cpu is
  -- I-Type
  constant OPC_LW :   std_logic_vector := "100011";
  constant OPC_SW :   std_logic_vector := "101011";
  constant OPC_BEQ :  std_logic_vector := "000100";
  constant OPC_ANDI : std_logic_vector := "001100";
  constant OPC_LUI :  std_logic_vector := "001111";
  constant OPC_ORI :   std_logic_vector := "001101";
  
  -- R-Type
  constant OPC_ADD :  std_logic_vector := "100000";
  constant OPC_OR :   std_logic_vector := "100101";
  constant OPC_SLT :  std_logic_vector := "101010";
  
   
  -- J-Type
  constant OPC_J :    std_logic_vector := "000010";
  constant OPC_JR :   std_logic_vector := "001000";
  
  constant OPTYPE_I : std_logic_vector := "00";
  constant OPTYPE_R : std_logic_vector := "01";
  constant OPTYPE_J : std_logic_vector := "11";
  
  -- ALU Operation 
  constant O_ADD : std_logic_vector(1 downto 0) := "00";
  constant O_SUB : std_logic_vector(1 downto 0) := "01";
  constant O_OR : std_logic_vector(1 downto 0) := "10";
  constant O_AND : std_logic_vector(1 downto 0) := "11";  
  
  function DECODE_TYPE(instruction : std_logic_vector(31 downTo 0)) 
  return std_logic_vector is 
    variable output : std_logic_vector(1 downTo 0);
  begin
    case instruction(31 downTo 26) is
      when OPC_LW =>
        output := OPTYPE_I;
      when OPC_SW =>
        output := OPTYPE_I;
      when OPC_BEQ =>
        output := OPTYPE_I;
      when OPC_ANDI =>
        output := OPTYPE_I;
      when OPC_LUI =>
        output := OPTYPE_I;
      when OPC_ORI =>
        output := OPTYPE_I;        
      when OPC_ADD =>
        output := OPTYPE_R;
      when OPC_OR =>
        output := OPTYPE_R;
      when OPC_SLT =>
        output := OPTYPE_R;
      when OPC_J =>
        output := OPTYPE_J;
      when OPC_JR =>
        output := OPTYPE_j;
      when others =>
        assert false report "Invalid OPCODE Provided" severity warning;
    end case;
    return output;
  end DECODE_TYPE;

  
  signal mem_write : bit := '0';
  signal mem_address : std_logic_vector(31 downTo 0) := "00000000000000000000000000000011";
  signal mem_data_in : std_logic_vector(31 downTo 0);
  signal mem_data_out : std_logic_vector(31 downTo 0);
  signal mem_reset : bit := '0';
  
  signal PC : std_logic_vector(31 downTo 0) := "00000000000000000000000000000000";
  
  signal IR : std_logic_vector(31 downTo 0);
  signal IR_write : bit := '0';
  
  signal ALU_A : std_logic_vector (31 downto 0) := "00000000000000000000000000000000";
  signal ALU_B : std_logic_vector (31 downto 0) := "00000000000000000000000000000000";
  signal ALU_CHOICE : std_logic_vector (1 downto 0) := "00";
  signal ALU_B_SHIFT : bit := '0';
  signal ALU_RESULT : std_logic_vector (31 downto 0);
  signal ALU_ZERO_DETECT : bit;
  
  signal REG_1 : std_logic_vector(4 downto 0) := "00000";
  signal REG_2 : std_logic_vector(4 downto 0) := "00000";
  signal REG_WRITE : std_logic_vector(4 downto 0) := "00000";
  signal REG_WRITE_DATA : std_logic_vector (31 downto 0);
  signal REG_WRITE_ENABLED : bit := '0';
  signal REG_1_OUT : std_logic_vector(31 downto 0);
  signal REG_2_OUT : std_logic_vector(31 downto 0);
  signal REG_RESET : bit := '0';
  
  begin
    mem_component : entity work.memory(behav) port map (mem_write, mem_address, mem_data_in, mem_data_out, mem_reset);
    ir_component : entity work.instruction_register(behav) port map (mem_data_out, IR, IR_write);
    alu_component : entity work.ALU_32(ALU) port map (ALU_A, ALU_B, ALU_CHOICE, ALU_B_SHIFT, ALU_RESULT, ALU_ZERO_DETECT);
    reg_component : entity work.registers(behav) port map (REG_1, REG_2, REG_WRITE, REG_WRITE_DATA, REG_WRITE_ENABLED, REG_1_OUT, REG_2_OUT, REG_RESET);
    
    update_simulator : process(PC, IR) is begin
      SIMULATOR_PC <= PC;
      SIMULATOR_IR <= IR;
    end process;
    
    main_process : process is 
      variable instruction_type: std_logic_vector(1 downTo 0);
      variable old_address : std_logic_vector (31 downto 0);
      variable old_r1 : std_logic_vector (4 downto 0);
      variable old_r2 : std_logic_vector (4 downto 0);
      variable instruction_rs : std_logic_vector (4 downto 0);
      variable instruction_rt : std_logic_vector (4 downto 0);
      variable instruction_rd : std_logic_vector (4 downto 0);
      variable instruction_address : std_logic_vector (25 downto 0);
      variable instruction_immediate : std_logic_vector (15 downTo 0);
      variable decoded_address : std_logic_vector (31 downto 0);
    begin
    loop        
      -- Allow simulator to move PC
      if SIMULATOR_PC_write = '1' then
        PC <= SIMULATOR_PC_in;
      end if;
      
      if SIMULATOR_RESET = '1' then
        mem_reset <= '1';
        REG_RESET <= '1';
        wait for 1 ns;
        mem_reset <= '0';
        REG_RESET <= '0';
      end if;
      
      if SIMULATOR_RUN = '1' then
        assert false report "Cycle Started" severity warning;
        
        -- (1) Load instruction from memory
        mem_address <= PC;
        wait for 1 ns;
        IR_write <= '1';
        IR_write <= transport '0' after 1 ns;

        -- (1a) Increment PC
        --PC <= std_logic_vector(unsigned(PC) + 4);
        
        ALU_A <= PC;
        ALU_B <= "00000000000000000000000000000100";
        ALU_CHOICE <= O_ADD;
        ALU_B_SHIFT <= '0';
        wait for 2 ns;
        PC <= ALU_RESULT;
        
        -- (2) Decode instruction type, Extract parts of insttuction
        instruction_type := DECODE_TYPE(IR);
        
        instruction_rs := IR(25 downTo 21);
        instruction_rt := IR(20 downTo 16);     
        instruction_rd := IR(15 downTo 11); 
       
        REG_1 <= instruction_rs;
        REG_2 <= instruction_rt;       
          
        case instruction_type is
          when OPTYPE_I =>
            instruction_immediate := IR(15 downTo 0); 
          when OPTYPE_R => null;
          when OPTYPE_J =>
            instruction_address := IR(25 downTo 0);
          when others =>
            assert false report "Error, Invalid OPCODE" severity warning;
        end case;
        
        wait for 2 ns;
        
        -- Memory Instructions
      if IR(31 downTo 26) = OPC_LW or IR(31 downTo 26) = OPC_SW then
        ALU_B <= REG_1_OUT;
        ALU_A <= "00000000000000000000000000000000";
        ALU_A(17 downTo 2) <= instruction_immediate;
        ALU_B_SHIFT <= '1'; 
        wait for 1 ns;
        ALU_B_SHIFT <= '0';
        mem_address <= ALU_RESULT;
        wait for 1 ns;
        
        -- LW - Read from memory
        case IR(31 downTo 26) is
          when OPC_LW =>
            -- Write back to register
            REG_WRITE <= instruction_rt;
            REG_WRITE_DATA <= mem_data_out;
            REG_WRITE_ENABLED <= '1';
            wait for 1 ns;
            REG_WRITE_ENABLED <= '0';
            
          when OPC_SW =>
            -- Write back to memory
            mem_data_in <= REG_2_OUT;
            mem_write <= '1';
            wait for 1 ns;
            mem_write <= '0';
            
          when others => null;
        end case;
        
      else 
        -- Non-memory Instructions
        ALU_A <= REG_1_OUT;
        ALU_B <= REG_2_OUT;
        ALU_B_SHIFT <= '0';
        REG_WRITE <= instruction_rd;
                  
        case instruction_type is
         when OPTYPE_R =>
          case IR(31 downTo 26) is
            when OPC_ADD =>
              ALU_CHOICE <= O_ADD;
              wait for 2 ns;
              REG_WRITE_DATA <= ALU_RESULT;
            when OPC_OR =>
              ALU_CHOICE <= O_OR;
             wait for 2 ns;
              REG_WRITE_DATA <= ALU_RESULT;
            when OPC_SLT =>
              ALU_CHOICE <= O_SUB;
              wait for 2 ns;
              if ALU_RESULT(31) = '0' then
                REG_WRITE_DATA <= "00000000000000000000000000000000";
              else
                REG_WRITE_DATA <= "00000000000000000000000000000001";
              end if;
            
            when others => null;
          end case;
         
          REG_WRITE_ENABLED <= '1';
          wait for 2 ns;
          REG_WRITE_ENABLED <= '0';
          
        when OPTYPE_I =>
          REG_WRITE <= instruction_rt;
          case IR(31 downTo 26) is
            when OPC_BEQ =>
              ALU_CHOICE <= O_SUB;
              wait for 2 ns;
              if ALU_ZERO_DETECT = '1' then
                ALU_A <= PC;
                ALU_B <= IEEE.Std_logic_arith.SXT(instruction_immediate, 32);
                ALU_CHOICE <= O_ADD;
                ALU_B_SHIFT <= '1';
                wait for 2 ns;
                ALU_B_SHIFT <= '0';
                PC <= ALU_RESULT;
              end if;
             when OPC_ORI =>
               ALU_B <= "00000000000000000000000000000000";
               ALU_B(15 downTo 0) <= instruction_immediate;
                         
               ALU_CHOICE <= O_OR;
               wait for 2 ns;
               REG_WRITE_DATA <= ALU_RESULT;              
               REG_WRITE_ENABLED <= '1';
               wait for 2 ns;
               REG_WRITE_ENABLED <= '0';
               
              when OPC_ANDI =>
               ALU_B <= "00000000000000000000000000000000";
               ALU_B(15 downTo 0) <= instruction_immediate;
                         
               ALU_CHOICE <= O_AND;
               wait for 2 ns;
               REG_WRITE_DATA <= ALU_RESULT;              
               REG_WRITE_ENABLED <= '1';
               wait for 2 ns;
               REG_WRITE_ENABLED <= '0';    
                 
              when OPC_LUI =>     
                REG_WRITE_DATA <= "00000000000000000000000000000000";
                REG_WRITE_DATA(31 downTo 16) <= instruction_immediate;
                REG_WRITE_ENABLED <= '1';
                wait for 2 ns;
                REG_WRITE_ENABLED <= '0';   
                          
            when others => null;
              
          end case; -- END CASE: OPTYPE == OPTYPE_I -> OPCODE
          
        when OPTYPE_J =>
          case IR(31 downTo 26) is
            when OPC_J =>
              ALU_CHOICE <= O_ADD;
              ALU_A <= "00000000000000000000000000000000";
              ALU_B <= "00000000000000000000000000000000";
              ALU_B_SHIFT <= '1';
              ALU_A(31 downTo 28) <= PC(31 downTo 28);
              ALU_B(25 downTo 0) <= instruction_address;
              wait for 2 ns;
              PC <= ALU_RESULT;    
            when OPC_JR =>
              PC <= REG_1_OUT;
            when others => null;
          end case; -- END CASE: OPTYPE == OPTYPE_J -> OPCODE
          
        when others => null; 
      end case; -- END CASE: OPTYPE
    end if;
        
        assert false report "Cycle Complete" severity warning;
        wait for 5 ns;
        
      else
        old_address := mem_address;
        old_r1 := REG_1;
        old_r2 := REG_2;
        
        mem_address <= SIMULATOR_MEM_ADDRESS;
        REG_1 <= SIMULATOR_REG_1;
        REG_2 <= SIMULATOR_REG_2;
        
        wait for 1 ns;
        SIMULATOR_MEM_DATA <= mem_data_out;
        SIMULATOR_REG_1_OUT <= REG_1_OUT;
        SIMULATOR_REG_2_OUT <= REG_2_OUT;
        
        wait for 1 ns;
        mem_address <= old_address;
        REG_1 <= old_r1;
        REG_2 <= old_r2;
      end if;
      
      wait for 1 ns;
    end loop;
    
  end process main_process;
  
end architecture behav;
